This repository contains R scripts associated with the results reported in "Imputation of Missing Values in Multi-View Data" (Van Loon, Fokkema, De Vos, Koini, Schmidt, & De Rooij, 2024) (https://doi.org/10.1016/j.inffus.2024.102524).

All simulations were performed in R (version 4.1.2) on a high-performance computing cluster running Cent OS (Stream 8) with Slurm Workload Manager (version 20.11).

## The following packages are required

* missForest (version 1.5)
* mice (version 3.14)
* mvs (version 1.0.2)
* glmnet (version 4.1-4)
* MOFA2 (version 1.8.0)
* basilisk (version 1.10.2)

## Overview of included files

The following files are included in this repository:

* blockcorrelate.R - A function used to generate features with a block-correlation structure
* apply_to_mri.R - A file used for generating missingness and then imputing the missing values in the multi-view MRI data.
* cca.R - A file used for running complete case analysis on a SLURM HPC cluster
* cda.R - A file used for running complete data analysis on a SLURM HPC cluster
* forest.R - A file used for running missForest on a SLURM HPC cluster
* figure_13.R - A file to generate Figure (A.)13
* meta_cv.R - A file used for running meta-level PMM on a SLURM HPC cluster (strategy 2)
* meta_forest.R - A file used for running meta-level missForest on a SLURM HPC cluster
* meta_mean.R -  A file used for running meta-level mean imputation on a SLURM HPC cluster
* meta_pmm.R - A file used for running meta-level PMM on a SLURM HPC cluster (strategy 1)
* mi.R - A file used for running mean imputation on a SLURM HPC cluster
* missingness_functions.R - A file containing functions for generating missingness and all imputation methods
* mofa.R - A file used for running MOFA imputation on a SLURM HPC cluster.
* sim_normal_views.R - A function used to simulate data
* sim_normal_views_beta.R - A function used to simulate data
* trying_mice.R - A file containing several experiments testing the feasibility of mice in high-dimensional data
* This README

The following files are NOT included in this repository:

* Shell scripts used for repeatedly calling the relevant R files. 
* Any used R packages. These packages can be found on CRAN or Bioconductor.
* The multi-view MRI data. 


