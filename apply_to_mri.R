# This script applies a chosen imputation method to the MRI data with structural or functional missingness
# Load the data and required software
library(mvs)
source("kFolds.R")
source("missingness_functions.R")
# load("multimodalMRIdata_v2_std.RData") # Unfortunately, the data cannot be publicly shared

# Missingness Proportion
miss_prop <- 0.75

# Missingness Type
miss_type <- "structural" # Change this to functional to set the functional views to be missing instead

# Number of folds to use in cross-validation:
nfolds <- 10

# Number of times to repeat the cross-validation procedure:
nreps <- 10

# Generate all combinations
reps <- 1:nreps
folds <- 1:nfolds
conds <- expand.grid(folds, reps)[, 2:1]
names(conds) <- c("rep_id", "fold_id")

# Generate the cross-validation folds
fold_list <- vector("list", nreps)
set.seed(110221)
for(i in 1:nreps){
  fold_list[[i]] <- kFolds(Alzheimer, nfolds)
}

# Generate the missingness
miss_list <- vector("list", nreps)
set.seed(051223)
for(i in 1:nreps){
  miss_list[[i]] <- sample(1:249, ceiling(miss_prop*249))
}

# Generate a vector of seeds
set.seed(120221)
seed_list <- sample(.Machine$integer.max/2, size = nrow(conds))

# Get the SLURM task ID
TID <- as.numeric(Sys.getenv("SLURM_ARRAY_TASK_ID"))

# Select corresponding fold IDS
rep_folds <- fold_list[[conds[TID, 1]]]
fold_id <- conds[TID, 2]

# Remove the missing data for CCA
missing_observations <- miss_list[[conds[TID, 1]]]
if(miss_type=="structural"){
  predictors[intersect(which(fold_id != rep_folds), missing_observations), viewIndexTop==1] <- NA
} else if(miss_type=="functional"){
  predictors[intersect(which(fold_id != rep_folds), missing_observations), viewIndexTop==3] <- NA
} else stop("miss_type must be structural or functional")

# Train the model
id <- list(xtrain=predictors[fold_id != rep_folds, ], ytrain=Alzheimer[fold_id != rep_folds])
set.seed(seed_list[TID])
time <- system.time(fit <- staplr_meta_mean(id, viewIndexIntermediate)) 
# staplr_meta_mean runs StaPLR with meta-level mean imputation. 
# To use a different type of imputation, simply use a different function from missingness_functions.R
# For example:
# staplr_meta_forest(id, viewIndexIntermediate) for meta level missForest, 
# staplr_meta_pmm(id, viewIndexIntermediate) for meta level PMM,
# etc.

# Save the results
results <- list()
results$TID <- TID
results$rep <- conds[TID, 1]
results$missing_observations <- missing_observations
results$miss_prop <- miss_prop
results$miss_type <- miss_type
results$preds <- predict(fit, predictors[fold_id == rep_folds, ])
results$ytest <- Alzheimer[fold_id == rep_folds]
results$coefs <- coef(fit)$`meta`[-1]
results$rep_folds <- rep_folds
results$fold_id <- fold_id
results$time <- time

save(results, file=paste0("/meta_mi_structural/rep",
                          conds[TID,1], "_fold", fold_id, ".RData")) # change the outcome directory according to the chosen missingness and imputation method
warnings()

