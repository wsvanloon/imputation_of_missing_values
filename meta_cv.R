source("blockcorrelate.R")
source("sim_normal_views_beta.R")
source("missingness_functions.R")
library(multiview)
library(mice)
#library(missForest)

# Repetitions
reps <- 1:100

# Sample size of complete data
n <- 1000

# Which view is irrelevant?
noise_view <- c(1,4)

# Which view has missingness?
missing_view <- c(1,4)

# What is the proportion of missingness?
missing_prop <- c(0.5, 0.9)

# Generate all combinations
conds <- expand.grid(reps, missing_prop, missing_view, noise_view)
names(conds) <- c("rep", "missing_prop", "missing_view", "noise_view")

# Generate a vector of seeds
set.seed(410422)
seed_list <- sample(.Machine$integer.max/2, size = nrow(conds))

# Get the SLURM task ID
TID <- as.numeric(Sys.getenv("SLURM_ARRAY_TASK_ID"))


# Generate data
set.seed(seed_list[TID])

mv <- c(5,50,500,5000)
view_index <- rep(1:4, mv)
abs_beta_full <- rep(2/sqrt(mv), mv)

if(conds$noise_view[TID]==0){
  abs_beta <- abs_beta_full
}else if(conds$noise_view[TID]==1){
  abs_beta <- abs_beta_full
  abs_beta[1:5] <- 0
}else if(conds$noise_view[TID]==2){
  abs_beta <- abs_beta_full
  abs_beta[6:55] <- 0
}else if(conds$noise_view[TID]==3){
  abs_beta <- abs_beta_full
  abs_beta[56:555] <- 0
}else if(conds$noise_view[TID]==4){
  abs_beta <- abs_beta_full
  abs_beta[556:5555] <- 0
}

complete_data <- sim_normal_views_beta(v = 4, 
                                       m = mv, 
                                       s=c(1,1,1,1), 
                                       ntrain=n, 
                                       ntest=1000, 
                                       abs_beta=abs_beta, 
                                       rw=0.5, 
                                       rb=0.2)

incomplete_data <- gm_train(complete_data, view_index, conds$missing_view[TID], conds$missing_prop[TID])

# Train a model using pmm imputation at the meta level, with m crossvalidations
time <- system.time(meta_cv_fit <- staplr_meta_cv(incomplete_data, view_index, 5))

# Save quantities required for calculating outcome metrics
results <- list()
results$TID <- TID
results$rep <- conds$rep[TID]
results$missing_prop <- conds$missing_prop[TID]
results$missing_view <- conds$missing_view[TID]
results$noise_view <- conds$noise_view[TID]
results$preds <- predict(meta_cv_fit, complete_data$xtest)
results$coefs <- coef(meta_cv_fit)$`meta`[-1]
results$ytest <- complete_data$ytest 
results$ptest <- complete_data$ptest
results$time <- time

save(results, file=paste0("/meta_cv/", TID, ".RData"))
warnings()